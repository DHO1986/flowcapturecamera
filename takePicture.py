import time
import picamera
import os

def start():
    with picamera.PiCamera() as camera:
        # create outputfolder if it does not exist
        if not os.path.exists('output'):
            os.makedirs('output')
     
        # save media into outputfolder using the rpis serial as unique identifier
        localpath = 'output/capture.jpg'
        camera.capture(localpath)
        
        print ">> RPI: picture taken"
        print ">> RPI: saved locally to {0}".format(localpath)
    
if __name__ == '__main__':
    start()

# import os
# import picam
# import tools

# def start():
#     # create outputfolder if it does not exist
#     if not os.path.exists('output'):
#         os.makedirs('output')
#  
#     # save media into outputfolder using the rpis serial as unique identifier
#     localpath = 'output/capture.jpg'
#     # localpath = 'output/{0}.jpg'.format(tools.getSerial())
#     pic = picam.takePhoto()
#     pic.save(localpath)
#     
#     print ">> RPI: picture taken"
#     print ">> RPI: saved locally to {0}".format(localpath)
#     
# if __name__ == '__main__':
#     start()


