from SocketServer import UDPServer, BaseRequestHandler
# own scripts
import takeVideo
import takePicture
import clear
import os.path

# BaseRequestHandler.request = (message, clientSocketObject)

myServer = ""
g_count = 0

class Handler(BaseRequestHandler):
    def handle(self):
        # setup socket with client
        socket = self.request[1]
        
        # capturing methods
        message = self.request[0]
        print "UDP message: " + message
        # debug info
        print "------------------------"
        print "task:", self.request[0]
        print "from:", self.client_address
        
        # this could be implemented in the UDPServer.py script
        # for advanced video settings
        dictio = eval(message) 
        print "Splitting up dictionary.."
        for key in dictio:
            print "> {0}: {1}".format(key, dictio[key])

if __name__ == "__main__":
    addr = ("", 5555)
    print "TEST: This UDP server accepts advanced messages"
    print "TEST: Incoming message is a dictionary with arbitrary structure"
    print "listening on {0}".format(addr)
    
    # listen on localhost:5555 and let Handler class handle requests
    myServer = UDPServer(addr, Handler)
    myServer.serve_forever()