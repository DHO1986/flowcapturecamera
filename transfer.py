'''''''''''''''''''''''''''''''''''''''''''''''''''
push data to client via sftp

can be driven in debug mode by adding arg '-debug'
'''''''''''''''''''''''''''''''''''''''''''''''''''
# note: there is a vise versa implementation on the
# server to get the data from the rpi


import sys
import os
import picam
import paramiko

# defaults
clientIP = "192.168.178.57"
clientOS = "nt"

# constants
REMOTE_PATH_UNIX = 'flowcapture/'
REMOTE_PATH_NT = r"%HOMEPATH%\\flowcapture\\"

if __name__ == '__main__':
    
    # parse args, overwrite defaults
    if len(sys.argv) == 3:
        clientIP = sys.argv[1]
        clientOS = sys.argv[2]
        # remotepath depending on client OS
        # supporting windows and unix based OS
        remotepath = REMOTE_PATH_NT if clientOS == "nt" else REMOTE_PATH_UNIX
    elif len(sys.argv) == 2:
        # leave defaults as they are
        if sys.argv[1] == "debug":
            pass
    else:
        print ">> RPI: cancel, missing inputs\n{input1}\n{input2}".format(
            input1="targetIP - IP to transfer media files",
            input2="os - identify client OS"
        )
        sys.exit(0)
 
    # create outputfolder if it does not exist
    if not os.path.exists('output'):
        print ">> RPI: there is no data to transfer"
        sys.exit(0)

    # transfer data to the client machine
    try:
        print ">> RPI: transfering data to client machine.."
        print ">> path: {0}".format(remotepath)
        
        # connect to client machine
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(clientIP, username="dominik", password="koksnutte", timeout=10)
   
        # transfer data
        sftp = client.open_sftp()
        sftp.put('output/*', remotepath)
        sftp.close()
        client.close()
        sys.exit(0)
        
    except IndexError:
        print ">> RPI: failed to send data"


