echo "updating raspberry.."
echo "warning: this may take several minutes"
sudo apt-get update -y && sudo apt-get upgrade -y

echo "checking for pip installation.."
sudo apt-get install python-pip -y

echo "fetching python devtools.."
sudo apt-get install libjpeg8-dev -y
sudo apt-get install python-dev -y

echo "fetching required python packages.."
cd flowcapturecamera
sudo pip install -r requirements.txt --upgrade

sudo pip install https://github.com/ashtons/picam/zipball/master#egg-picam

echo "ready to use for flow capture"
