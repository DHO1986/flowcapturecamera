import os
import glob

def start():
    files = glob.glob('output/*')
    for f in files:
        os.remove(f)

if __name__ == '__main__':
    start()