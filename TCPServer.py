# performing a TCP connection delay and reliability test
#!/usr/bin/env python

# own scripts
import takeVideo
import takePicture
import clear
import os.path
import picamera

import socket

TCP_IP = '127.0.0.1'
TCP_PORT = 80
BUFFER_SIZE = 20  # usually 1024, but we want fast response

# create an INET, STREAMing socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# bind the socket to a public host, and a well-known port
s.bind((socket.gethostname(), 80))
# become a server socket
s.listen(1)

print "setting up connection .."
conn, addr = s.accept()
print 'connection address: {0}'.format(addr)

counter = 0
while 1:
    data = conn.recv(BUFFER_SIZE)
    if not data: break
    print 'remote: received data: {0}'.format(data)
    
    # take picture when data was received
    with picamera.PiCamera() as camera:
        # create outputfolder if it does not exist
        if not os.path.exists('output'):
            os.makedirs('output')
     
        # save media into outputfolder using the rpis serial as unique identifier
        localpath = 'output/tcpTest{0}.jpg'.format(counter)
        counter += 1
        camera.capture(localpath)
        
        print ">> RPI: picture taken"
        print ">> RPI: saved locally to {0}".format(localpath)
        
    # echo    
    conn.send(data)
conn.close()