from SocketServer import UDPServer, BaseRequestHandler
# own scripts
import takeVideo
import takePicture
import clear
import os.path

# BaseRequestHandler.request = (message, clientSocketObject)

myServer = ""
g_count = 0

class Handler(BaseRequestHandler):
    def handle(self):
        # setup socket with client
        socket = self.request[1]
        
        # capturing methods
        message = self.request[0]
        if message == "image":
            socket.sendto("remote: taking picture", self.client_address)
            takePicture.start()
        elif message == "video":
            socket.sendto("remote: recording video", self.client_address)
            takeVideo.start()
        elif message == "clear":
            socket.sendto("remote: clearing output folder", self.client_address)
            clear.start()
        # validate UDP packageloss rate
        elif message == "test":
            socket.sendto("remote: answer {0}".format(g_count), self.client_address)
            global g_count
            g_count += 1
        elif message == "testclear":
            socket.sendto("remote: test reseted", self.client_address)
            global g_count
            g_count = 0
        elif message == "testresult":
            socket.sendto("remote: answered {0} of 10000 requests".format(g_count), self.client_address)
        else:
            socket.sendto("remote: unknown message: '{0}'".format(message), self.client_address)
            
        # debug info
        print "------------------------"
        print "message:", self.request[0]
        print "from:", self.client_address

if __name__ == "__main__":
    addr = ("", 5555)
    print "listening on {0}".format(addr)
    
    # listen on localhost:5555 and let Handler class handle requests
    myServer = UDPServer(addr, Handler)
    myServer.serve_forever()