# rasberrypi slave #

## Tasks ##
* synced and transform calibrated capturing
* send files back to client
* taskrunner, no user interface

## Note ##
* the remote calls from client will generate an output folder in the home directory of the rpi. this differs from executing the script locally, which creates the output folder inside the project folder.