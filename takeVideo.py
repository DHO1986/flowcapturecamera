import os
import picam
import tools

def alternate():
    import picamera
    from time import sleep
    
    camera = picamera.PiCamera()
    camera.sharpness = 0
    camera.contrast = 0
    camera.brightness = 50
    camera.saturation = 0
    camera.ISO = 0
    camera.video_stabilization = False
    camera.exposure_compensation = 0
    camera.exposure_mode = 'none'
    camera.meter_mode = 'average'
    camera.awb_mode = 'none'
    camera.image_effect = 'none'
    camera.color_effects = None
    camera.rotation = 0
    camera.hflip = False
    camera.vflip = False

    localpath = 'output/capture.h264'
    camera.start_recording(localpath)
    sleep(5)
    camera.stop_recording()

def start():
    # create outputfolder if it does not exist
    if not os.path.exists('output'):
        os.makedirs('output')
 
    # save media into outputfolder using the rpis serial as unique identifier
    localpath = 'output/capture.h264'
    
#     # image presets
#     picam.config.exposure = picam.MMAL_PARAM_EXPOSUREMODE_OFF
    picam.config.meterMode = picam.MMAL_PARAM_EXPOSUREMETERINGMODE_BACKLIT
    picam.config.awbMode = picam.MMAL_PARAM_AWBMODE_OFF
    picam.config.ISO = 0 
    picam.config.videoFramerate = 25
    
    # record
    # localpath = 'output/{0}.h264'.format(tools.getSerial())
    picam.recordVideoWithDetails(localpath,640,480,10000)  
    
    print ">> RPI: video recorded"
    print ">> RPI: saved locally to {0}".format(localpath)
    
if __name__ == '__main__':
    start()
    #alternate()
